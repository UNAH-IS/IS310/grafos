/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafosdirigidos;

/**
 *
 * @author enrique
 */
public class GrafoMatrizDirigido {

    private int vertice;
    private int[][] matrizAdyacencia;

    // constructor  
    public GrafoMatrizDirigido(int vertice) {
        this.vertice = vertice;

        matrizAdyacencia = new int[vertice][vertice];

        for (int i = 0; i < vertice; i++) {
            for (int j = 0; j < vertice; j++) {
                matrizAdyacencia[i][j] = 0;
            }
        }
    }

    public void agregarArista(int origen, int destino) {
        // Evitar los bucles en grafos no dirigidos        
        if (origen != destino) {
            this.matrizAdyacencia[origen][destino] = 1;
        }
    }

    public void imprimir() {
        for (int i = 0; i < vertice; i++) {
            for (int j = 0; j < vertice; j++) {
                System.out.print(this.matrizAdyacencia[i][j] + "\t");
            }
            
            System.out.print("\n");
        }
    }
    
    public static void main(String[] args) {
        GrafoMatrizDirigido grafo = new GrafoMatrizDirigido(5);
        
        grafo.agregarArista(0, 1);
        grafo.agregarArista(0, 4);
        grafo.agregarArista(1, 2);
        grafo.agregarArista(1, 3);
        grafo.agregarArista(1, 4);
        grafo.agregarArista(2, 3);
        grafo.agregarArista(3, 4);

        grafo.imprimir();
    }
}
