/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafosdirigidos;

import java.util.LinkedList;

public class GrafoListaDirigido {

    private int vertice;
    private LinkedList<Integer>[] listaAdyacencia;

    // constructor  
    public GrafoListaDirigido(int vertice) {
        this.vertice = vertice;
        listaAdyacencia = new LinkedList[vertice];

        for (int i = 0; i < vertice; i++) {
            listaAdyacencia[i] = new LinkedList<>();
        }
    }

    void agregarArista(int origen, int destino) {
        this.listaAdyacencia[origen].add(destino);
    }

    void imprimir() {
        for (int v = 0; v < this.vertice; v++) {
            System.out.printf("[%d]: ", v);
            System.out.print("primero");

            for (Integer elemento : this.listaAdyacencia[v]) {
                System.out.print(" -> " + elemento);
            }

            System.out.println("\n");
        }
    }

    public static void main(String args[]) {
        GrafoListaDirigido grafo = new GrafoListaDirigido(5);
        
        grafo.agregarArista(0, 1);
        grafo.agregarArista(0, 4);
        grafo.agregarArista(1, 2);
        grafo.agregarArista(1, 3);
        grafo.agregarArista(1, 4);
        grafo.agregarArista(2, 3);
        grafo.agregarArista(3, 4);

        grafo.imprimir();
    }
} 