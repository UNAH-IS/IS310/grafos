/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafosnodirigidos;

import java.util.LinkedList;

public class GrafoListaND {

    private int vertice;
    private LinkedList<Integer>[] listaAdyacencia;

    // constructor  
    public GrafoListaND(int vertice) {
        this.vertice = vertice;

        listaAdyacencia = new LinkedList[vertice];

        for (int i = 0; i < vertice; i++) {
            listaAdyacencia[i] = new LinkedList<>();
        }
    }

    void agregarArista(int origen, int destino) {
        this.listaAdyacencia[origen].add(destino);

        // Como el grado no dirigido se agrega un segundo enlace
        this.listaAdyacencia[destino].add(origen);
    }

    void eliminarArista(int origen, int destino) {
        Integer objOrigen = origen;
        Integer objDestino = destino;

        this.listaAdyacencia[origen].remove(objDestino);
        this.listaAdyacencia[destino].remove(objOrigen);
    }

    void eliminarVertice(int dato) {
        // Se elimina la lista de adyacencia del valor
        this.listaAdyacencia[dato] = null;
        Integer objDato = dato;

        // Elimina o intenta eliminar las posibles adyacencias en otros nodos
        for (LinkedList<Integer> sublista : this.listaAdyacencia) {
            if (sublista != null && !sublista.isEmpty()) {
                sublista.remove(objDato);
            }
        }
    }
    
    boolean sonAdyacentes(int origen, int destino) {
        // Revisar si en ambas listas de adyacencia se encuentran los vertices
        // Se revisan los dos extremos porque es un grafo no dirigido
        int indiceOrigen = this.listaAdyacencia[origen].indexOf(destino);
        int indiceDestino = this.listaAdyacencia[destino].indexOf(origen);
        
        // indexOf retorna -1 en caso de no existir el elemento.
        return (indiceOrigen != -1 && indiceDestino != -1);
    }

    void imprimir() {
        int i = 0;

        for (LinkedList<Integer> sublista : this.listaAdyacencia) {
            System.out.printf("[%d]: ", i++);

            if (sublista == null) {
                System.out.print("null");
            } else {
                System.out.print("Primero");
                
                for (Integer elemento : sublista) {
                    System.out.print(" -> " + elemento);
                }
            }

            System.out.print("\n");
        }
    }

    public static void main(String args[]) {
        GrafoListaND grafo = new GrafoListaND(5);
        grafo.agregarArista(0, 1);
        grafo.agregarArista(0, 4);
        grafo.agregarArista(1, 2);
        grafo.agregarArista(1, 3);
        grafo.agregarArista(1, 4);
        grafo.agregarArista(2, 3);
        grafo.agregarArista(3, 4);

        System.out.println("Original");
        grafo.imprimir();
        
        System.out.print("Son adyacentes los vertices 1 y 3: ");
        System.out.println(grafo.sonAdyacentes(1, 3));
        
        System.out.print("Son adyacentes los vertices 0 y 2: ");
        System.out.println(grafo.sonAdyacentes(0, 2));
        
        System.out.println("Eliminar arista {1, 2}");
        grafo.eliminarArista(1, 2);
        grafo.imprimir();

        System.out.println("Eliminar vertice: 1");
        grafo.eliminarVertice(1);
        grafo.imprimir();
    }
} 
// This code is contributed by Sumit Ghosh 
